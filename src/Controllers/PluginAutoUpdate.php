<?php

namespace PxlwpAutoUpdate\Controllers;

/**
* class Envato
*
* @package PxlwpEnvatoApi\Controllers
*/
class PluginAutoUpdate
{

    /**
    * The api url
    *
    * @var string
    */
    private $api_url = 'https://updates.pixolette.com/pixolette-api';

    /**
    * The plugin path
    *
    * @var string
    */
    private $plugin_path;

    /**
    * The plugin basename (for example: 'wp-jobs-board/wp-jobs-board.php')
    *
    * @var string
    */
    private $plugin_basename;

    /**
    * The plugin full path
    *
    * @var string
    */
    private $plugin_full_path;

    /**
    * The plugin licence key
    *
    * @var string
    */
    private $license_key;

    /**
    * The plugins that needs to be skipped
    *
    * @var string
    */
    private $skip_plugins = array(
        'pixolette-envato/pixolette-envato.php'
    );


    /**
    * Initialize a new instance of AutoUpdate class
    *
    * @param string $plugin_full_path
    */
    function __construct($plugin_full_path)
    {
        // check if function is called in WordPress. Stop if not.
        if (!defined('ABSPATH')) {
            return;
        }

        $this->plugin_path = plugin_dir_path($plugin_full_path);
        $this->plugin_basename = plugin_basename($plugin_full_path);
        $this->plugin_full_path = $plugin_full_path;

        if (in_array($this->plugin_basename, $this->skip_plugins)) {
            return;
        }

        $tr = get_transient('pxlwp-plugin-info-' . $this->plugin_basename);

        // check if plugin is active.
        if (!$this->currentPluginIsActive()) {
            return;
        }

        if (!function_exists( 'get_plugin_data')) {
            include_once(ABSPATH . 'wp-admin/includes/plugin.php');
        }
        $this->plugin_data = get_plugin_data($this->plugin_full_path);

        // admin
		if (is_admin()) {
			add_action('in_plugin_update_message-' . $this->plugin_basename, array($this, 'customPluginUpdateMessage'), 10, 2);
		}

        // append update information to transient
		add_filter('pre_set_site_transient_update_plugins', array($this, 'customPluginsTransient'), 10, 1);

        // modify plugin data visible in the 'View details' popup
		add_filter('plugins_api', array($this, 'getRemotePluginDetails'), 10, 3);

        add_filter('http_request_host_is_external', array($this, 'allowPluginUpdateFromCustomHost'), 10, 3);
    }

    /**
    * Function to allow WordPress updates from custom url
    *
    * @param bool $allow
    * @param string $host
    * @param string $url
    *
    * @return bool
    */
    function allowPluginUpdateFromCustomHost($allow, $host, $url)
    {
        if ('updates.pixolette.com' == $host || 'wp.pixolette.test' == $host) {
            $allow = true;
        }

        return $allow;
    }

    /**
    * Function to check if current plugin is active
    *
    * @return bool
    */
    function currentPluginIsActive()
    {
        $basename = $this->plugin_basename;
        if (!function_exists( 'is_plugin_active')) {
            include_once(ABSPATH . 'wp-admin/includes/plugin.php');
        }

        return is_plugin_active($basename);
    }

    /**
    * Function to display custom message when new version is available
    *
    * @param mixed $plugin_data
    * @param mixed $response
    */
    public function customPluginUpdateMessage($plugin_data, $response)
    {
        $license_key_status = $this->getLicenseKeyStatus();

        if (isset($license_key_status['license-code']) && isset($license_key_status['license-status']) && $license_key_status['license-status'] == 'valid' && $license_key_status['license-code'] == 'valid') {
            return;
        }
        if ('invalid' == $license_key_status['license-code']) {
            echo '<br />' . sprintf( __('The Purchase Code you\'ve submitted is not valid. If you don\'t have a Purchase Code, please see <a href="%s">details & pricing</a>.', 'pixolette-product'), 'https://wp.pixolette.com/wordpress-plugins/' );
        } elseif ('expired' == $license_key_status['license-status']) {
            echo '<br />' . sprintf( __('The Purchase Code you\'ve submitted is expired. You can update the plugin, but can\'t get support. If you want to renew the support, please see <a href="%s">details & pricing</a>.', 'pixolette-product'), 'https://wp.pixolette.com/wordpress-plugins/' );
        } else {
            echo '<br />' . sprintf( __('To enable updates, please enter Envato Purchase Code on the plugin page. If you don\'t have a Purchase Code, please see <a href="%s">details & pricing</a>.', 'pixolette-product'), 'https://wp.pixolette.com/wordpress-plugins/' );
        }
    }

    /**
    * Function to customize the plugin transient
    *
    * @param mixed $transient
    *
    * @return mixed $transient
    */
    public function customPluginsTransient($transient)
    {
        if (!isset($transient->response)) {
			return $transient;
		}

        // Get the remote version
        $remote_plugin_data = $this->getRemotePluginData();
        if (!is_wp_error($remote_plugin_data) && isset($remote_plugin_data->new_version) && $remote_plugin_data->new_version) {
            // If a newer version is available, add the update
            if (version_compare($this->plugin_data['Version'], $remote_plugin_data->new_version, '<')) {
        		$transient->response[$this->plugin_basename] = $remote_plugin_data;
            }
        }

        return $transient;
    }

    /**
    * Function to get the product remote data
    *
    * @param mixed $result
    * @param mixed $action
    * @param mixed $args
    *
    * @return mixed $transient
    */
    public function getRemotePluginDetails($result, $action = null, $args = null)
    {
        // vars
        $plugin = false;

        // only for 'plugin_information' action
        if ($action !== 'plugin_information') {
            return $result;
        }

        if ($args->slug == $this->plugin_basename) {
            $plugin = true;
        }

        if (!$plugin) {
            return $result;
        }

        $transient_name = 'pxlwp-plugin-details-' . $this->plugin_basename;

        // delete transient (force-check is used to refresh)
		if (isset($_GET['force-check']) && 1 == $_GET['force-check']) {
			delete_transient($transient_name);
		}

        // try transient
		$transient = get_transient($transient_name);
		if (false !== $transient) {
            return $transient;
        }

        // data that request update
        $udata = array(
            'pixaction' => 'product-details',
            'product-slug' => $this->plugin_basename,
            'website' => home_url(),
        );

        $response = $this->request('post', '/', $udata);
        if ($response && !is_wp_error($response)) {
            if (isset($response->success) && 1 == $response->success && isset($response->product)) {
                $transient = $response->product;
                $transient->icons = (array) $transient->icons;
                $transient->banners = (array) $transient->banners;
                $transient->sections = (array) $transient->sections;
                // update transient
        		set_transient($transient_name, $transient, 12 * HOUR_IN_SECONDS);

                return $transient;
            }
        }
    }

    /**
    * Function to get the product remote data
    *
    * @return mixed $transient
    */
    public function getRemotePluginData()
    {
        $transient_name = 'pxlwp-plugin-info-' . $this->plugin_basename;

        // delete transient (force-check is used to refresh)
		if (isset($_GET['force-check']) && 1 == $_GET['force-check']) {
			delete_transient($transient_name);
		}

        // try transient
		$transient = get_transient($transient_name);
		if (false !== $transient) {
            return $transient;
        }

        $license_key = $this->getLicenseKey();
        // data that request update
        $udata = array(
            'pixaction' => 'product-update-data',
            'license-code' => $license_key,
            'product-slug' => $this->plugin_basename,
            'website' => home_url(),
        );

        $response = $this->request('post', '/', $udata);

        if ($response && !is_wp_error($response)) {
            if (isset($response->success) && 1 == $response->success && isset($response->data)) {
                $transient = $response->data;
                // update transient
        		set_transient($transient_name, $transient, 12 * HOUR_IN_SECONDS);

                return $transient;
            }
        }
    }

    /**
    * Function to call API and retrieve data
    *
    * @param string $method
    * @param string $query
    * @param array $body
    * @param bool $return_array
    *
    * @return string $json
    */
    function request($method = 'post', $query = '?pixaction=product-version', $body = array(), $return_array = false)
    {
		$url = $this->api_url . $query;

        $raw_response = wp_remote_post($url, array(
            'timeout' => 10,
            'body' => $body
        ));

		// wp error
		if (is_wp_error($raw_response)) {
			return $raw_response;
			// http error
		} elseif (200 != wp_remote_retrieve_response_code($raw_response)) {
			return new \WP_Error('server_error', wp_remote_retrieve_response_message($raw_response));
		}

		// decode response
        if ($return_array) {
            $json = json_decode(wp_remote_retrieve_body($raw_response), true);
        } else {
            $json = json_decode(wp_remote_retrieve_body($raw_response));
        }

		// return
		return $json;
	}

    /**
    * Function to get the licence code for current product
    *
    * @return string $license_key
    */
    public function getLicenseKey()
    {

        $license_slug = 'pxlwp-purchase-code-' . $this->plugin_basename;

        $this->license_key = get_site_option( $license_slug );

        return $this->license_key;
    }

    /**
    * Function to get the licence code for current product
    *
    * @return mixed $transient
    */
    public function getLicenseKeyStatus()
    {
        if (!$this->license_key) {
            $license_key = $this->getLicenseKey();
        }

        if (!$this->license_key || ! trim($this->license_key)) {
            return;
        }

        $transient_name = 'pxlwp-license-status-' . $this->license_key;
        $transient = get_transient($transient_name);
        if (!$transient) {
            $data = array(
                'pixaction' => 'verify-license',
                'license-code' => $this->license_key,
                'product-slug' => $this->plugin_basename,
                'website' => home_url(),
            );
            $response = $this->request('post', '/', $data);
            if ($response && !is_wp_error($response)) {
                $transient = (array) $response;
                set_transient($transient_name, $transient, 24 * HOUR_IN_SECONDS);
            }
        }

        return $transient;
    }

}
